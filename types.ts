
export type ContentfulSys = {
    id: string;
}

export type FieldType = 'Symbol' | 'Text' | 'Integer' | 'Number' | 'Date' | 'Boolean' | 'Object' | 'Location' | 'RichText' | 'Array' | 'Link'
export type LinkMimetype = 'attachment' | 'plaintext' | 'image' | 'audio' | 'video' | 'richtext' |
    'presentation' | 'spreadsheet' | 'pdfdocument' | 'archive' | 'code' | 'markup'

export type FieldValidation = {
    /** Takes an array of content type ids and validates that the link points to an entry of that content type. */
    linkContentType?: string[],
    /** Takes an array of values and validates that the field value is in this array. */
    in?: string[] | number[],
    /** Takes a MIME type group name and validates that the link points to an asset of this group. */
    linkMimetypeGroup?: LinkMimetype[],
    /** Takes min and/or max parameters and validates the size of the array (number of objects in it). */
    size?: { max?: number, min?: number },
    /** Takes min and/or max parameters and validates the range of a value. */
    range?: { max?: number, min?: number },
    /** Takes a string that reflects a JS regex and flags, validates against a string. See JS reference for the parameters. */
    regexp?: { pattern: string, flags?: string },
    /** Validates that there are no other entries that have the same field value at the time of publication. */
    unique?: true,
    /** Validates that a value falls within a certain range of dates. */
    dateRange?: { min?: string, max?: string },
    /** Validates that an image asset is of a certain image dimension. */
    assetImageDimensions?: { width: { min?: number, max?: number }, height: { min?: number, max?: number } }
    /** Validates that an asset is of a certain file size. */
    assetFileSize?: { max?: number, min?: number },

    message?: string

    /** Other validations */
    [validation: string]: any
}

export type ContentfulField = {
    id?: string

    /** (required) – Field name. */
    name?: string
    /** (required) – Field type */
    type: FieldType

    /** Object (required for type 'Array') – Defines the items of an Array field. */
    items?: ContentfulField

    /** (required for type 'Link') – Type of the referenced entry. Can take the same values as the ones listed for type above. */
    linkType?: 'Asset' | 'Entry'
    /** Sets the field as required. */
    required?: boolean
    /** Validations for the field. */
    validations?: Array<FieldValidation>

    /** Sets the field as localized. */
    localized?: boolean
    /** Sets the field as disabled, hence not editable by authors. */
    disabled?: boolean
    /** Sets the field as omitted, hence not sent in response. */
    omitted?: boolean
    /** Sets the field as deleted. Requires to have been omitted first. You may prefer using the deleteField method. */
    deleted?: boolean
}

export type ContentfulExportContentType = {
    sys: ContentfulSys;
    displayField: string;
    name: string;
    description: string;
    fields: ContentfulField[]
};
export type WidgetSettingsValue = number | boolean | string | undefined
export type ContentfulExportEditorInterfaceConfigSettings = {
    /** This help text will show up below the field. */
    helpText?: string
    /** (only for fields of type boolean) Shows this text next to the radio button that sets this value to true. Defaults to “Yes”. */
    trueLabel?: string
    /** (only for fields of type boolean) Shows this text next to the radio button that sets this value to false. Defaults to “No”. */
    falseLabel?: string
    /** (only for fields of type rating) Number of stars to select from. Defaults to 5. */
    stars?: number
    /** (only for fields of type datePicker) – One of "dateonly", "time", "timeZ" (default). Specifies whether to show the clock and/or timezone inputs. */
    format?: 'dateonly' | 'time' | 'timeZ'
    /** (only for fields of type datePicker) – Specifies which type of clock to use. Must be one of the strings "12" or "24" (default). */
    ampm?: '12' | '24'
    /** (only for References, many) Select whether to enable Bulk Editing mode */
    bulkEditing?: boolean
    /** (only for fields of type slugEditor) – Specifies the ID of the field that will be used to generate the slug value. */
    trackingFieldId?: string

    /** Instance settings for the sidebar widget as key-value pairs. */
    [setting: string]: WidgetSettingsValue
}

export type ContentfulExportEditorInterfaceConfig = {
    fieldId: string;
    settings?: ContentfulExportEditorInterfaceConfigSettings,
    widgetId: string;
    widgetNamespace: 'builtin' | 'extension' | 'app';
};

export type ContentfulExportEditorInterface = {
    sys: {
        contentType: {
            sys: {
                id: string;
            }
        }
    };
    controls: ContentfulExportEditorInterfaceConfig[];
}

export type ContentfulExport = {
    contentTypes: ContentfulExportContentType[];
    editorInterfaces: ContentfulExportEditorInterface[];
};
