import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import { readFileSync, writeFileSync } from 'fs';
import { ContentfulExport, ContentfulExportEditorInterface, ContentfulExportEditorInterfaceConfig } from './types';

yargs(hideBin(process.argv))
.command('$0 <file>', 'create migration from contentful export', (yargs) => {
    yargs
    .positional('file', {
        describe: 'file to create definition from',
        type: 'string'
    })
    .option('o', {
        alias: 'offset',
        default: 1,
        describe: 'version offset',
        type: 'number'
    })
    .option('d', {
        alias: 'directory',
        type: 'string',
        describe: 'directory to write files to'
    })
}, (argv) => {
    const template = readFileSync('migration-template.ts.dist', 'utf-8');
    const file = readFileSync(argv.file as string, 'utf-8');
    const exportJson: ContentfulExport = JSON.parse(file);
    let offset = argv.offset as number;
    const numberFormatter = Intl.NumberFormat('de-AT', {minimumIntegerDigits: 2});


    exportJson.contentTypes.forEach((contentType) => {
        const contentTypeId = contentType.sys.id;

        let migrationAsString = `
    const ${contentTypeId} = migration.createContentType('${contentTypeId}', {
        name: '${contentType.name}',
        displayField: '${contentType.displayField}',
        description: '${contentType.description}',
    });
  `
        const editorInterface: ContentfulExportEditorInterface = exportJson.editorInterfaces.filter((editorInterface) => editorInterface.sys.contentType.sys.id === contentTypeId).pop() as ContentfulExportEditorInterface;

        contentType.fields.forEach((field) => {
            const fieldId = field.id;
            delete field.id;

            migrationAsString += `
    ${contentTypeId}.createField('${fieldId}', ${JSON.stringify(field)});
            `;

            const editorInterfaceForField = editorInterface.controls.filter((control) => control.fieldId === fieldId).pop();
            if (editorInterfaceForField && editorInterfaceForField.widgetNamespace && editorInterfaceForField.fieldId) {
                migrationAsString += `
    ${contentTypeId}.changeFieldControl('${fieldId}', '${editorInterfaceForField.widgetNamespace}', '${editorInterfaceForField.widgetId}', ${editorInterfaceForField.settings ? JSON.stringify(editorInterfaceForField.settings) : '{}'});                
                `
            }
        });


        const fileContent = template.replace('###CONTENT###', migrationAsString);
        const filename = argv.directory ? `${argv.directory}/${numberFormatter.format(offset)}-${contentTypeId}.ts` : `${numberFormatter.format(offset)}-${contentTypeId}.ts`;
        console.log(filename);
        writeFileSync(filename, fileContent);
        offset++;
    })
})
.parse()
